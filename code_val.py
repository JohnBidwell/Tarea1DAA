import numpy as np
import matplotlib.pyplot as plt

def plot_rand():
    points = []
    points_rand = np.random.rand(10, 2)   # 30 random points in 2-D
    x_rand = points_rand[:,0]
    y_rand = points_rand[:,1]
    plt.plot(x_rand, y_rand, 'o')
    plt.show()
    j = (len(x_rand))
    i = 0
    while(i < j):
        points.append([x_rand[i],y_rand[i]])
        i = i+1
    return points
    
puntos = plot_rand()
print(puntos,'\n')
puntos = sorted(puntos)
print(puntos,'\n')
def tangent(O,A,B):
    C = ((A[0]-O[0])*(B[1]-O[1])) - ((A[1]-O[1])*(B[0]-O[1]))
    return C 
def merge(HA,HB):
    lo = HA
    up = HA
    for b in HB:
        a = -1
        while tangent(up[a-1],up[a],b > 0):
            up.pop()
            up.append(b)
            a = a+1

        a = -1
        while tangent(lo[a-1],lo[a],b) < 0 :
            lo.pop()
            lo.append(b)
            a = a+1

def convex_hull(S):
    if len(S) <= 3:
        HC = S
    else:
        HA = convex_hull(S[0:len(S)/2])
        HB = convex_hull(S[(len(S)/2)+1:len(S)-1)])
        HC = merge(HA,HB)
    return HC

