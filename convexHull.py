import numpy as np
import matplotlib.pyplot as plt
import math
import unittest


def plot_rand(npoints):
    points_rand = np.random.rand(npoints, 2)   # 100 random points in 2-D (n puntos, dimensiones)
    return points_rand

def determinant(p0, p1, p2):
    sum1 = p1[0]*p2[1]+p0[0]*p1[1]+p2[0]*p0[1]
    sum2 = p1[0]*p0[1]+p2[0]*p1[1]+p0[0]*p2[1]
    return sum1-sum2

def onSegment(p0, p1, p2):
    if determinant(p0, p1, p2)<0:
        return 0 #Sentido antihorario
    else:
        return 1 #Sentido horario si >0, si 0, son colineales

def distance(p1, p2):
    return math.sqrt((p2[0]-p1[0])**2 + (p2[1]-p1[1])**2)

def perimetro(points):
    final=0
    for i in range(1,len(points)):
        p1=points[i-1]
        p2=points[i]
        final+=distance(p1,p2)
    final+=distance(points[0],points[-1])
    return final

def convexHull(points):
    points.sort()
    left = [points[0], points[1]]
    for i in range(2, len(points)//2, 1):
        left.append(points[i])
        while len(left) > 2 and not onSegment(left[0], left[1], left[2]):
            left.pop()
    right = [points[(len(points)//2)+1], points[(len(points)//2)+2]]
    for i in range (len(points)//2+3, len(points), 1):
        right.append(points[i])
        while len(right) > 2 and not onSegment(right[0], right[1], right[2]):
            right.pop()


    return left[:-1]+right[:-1]

test=[(0,0),(0,3),(3,3),(3,0), (1,1), (1,2), (1,3)]


#puntos=plot_rand2(10)
print(test)
new_puntos=convexHull(test)
print(perimetro(new_puntos))

class TestConvexHull(unittest.TestCase):
    def test_convexHull(self):
        tamanos = [10000, 250000, 500000, 1000000, 4000000, 8000000, 16000000]
        for i in range(len(tamanos)):
            print("Test for {tamano} points".format(tamano=tamanos[i]))
            points = plot_rand(tamanos[i])
            perimetro = convexHull(points)
            print(perimetro)

#if	__name__=='__main__':
#    unittest.main()
