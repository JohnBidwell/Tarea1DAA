import numpy as np
import matplotlib.pyplot as plt
import math
import unittest

def convex_hull(points):
    points.sort()
    n=len(points)
    if n <= 1:
        return points

    lower = []
    for p in points:
        while len(lower) >= 2 and cruz(lower[-2], lower[-1], p) <= 0:
            lower.pop()
        lower.append(p)

    upper = []
    for p in reversed(points):
        while len(upper) >= 2 and cruz(upper[-2], upper[-1], p) <= 0:
            upper.pop()
        upper.append(p)

    return lower[:-1] + upper[:-1] #porque se repite el ultimo con el primero del otro

def cruz(o, a, b):
    return (a[0] - o[0]) * (b[1] - o[1]) - (a[1] - o[1]) * (b[0] - o[0]) # 0 colineales; negativo clockwise; positivo counter-clock

def plot_rand(n):
    points = []
    points_rand = np.random.rand(n, 2)   # 30 random points in 2-D
    x_rand = points_rand[:,0]
    y_rand = points_rand[:,1]
    plt.plot(x_rand, y_rand, 'o')
    plt.show()
    j = (len(x_rand))
    i = 0
    while(i < j):
        points.append([x_rand[i],y_rand[i]])
        i = i+1
    return points

def distance(p1, p2):
    return math.sqrt((p2[0]-p1[0])**2 + (p2[1]-p1[1])**2)

def perimetro(puntos):
    final=0
    for i in range(1,len(puntos)):
        p1=puntos[i-1]
        p2=puntos[i]
        final+=distance(p1,p2)
    final+=distance(puntos[0],puntos[-1])
    return final

class TestConvexHull(unittest.TestCase):
    def test_convex_hull(self):
        tamanos = [10000, 250000, 500000, 1000000, 4000000, 8000000, 16000000]
        for i in range(len(tamanos)):
            print("Test for {tamano} points".format(tamano=tamanos[i]))
            points = plot_rand(tamanos[i])
            CH = convex_hull(points)
            print(perimetro(CH))

if	__name__=='__main__':
    unittest.main()
